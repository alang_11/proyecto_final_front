/**
 *
 * # cells-login-view
 *
 * ![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
 *
 * [Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)
 *
 * `cells-login-view` contains fields, buttons and links to show a complete login process view. It is mainly composed by a header image, `cells-credentials-form` for the login form, and `cells-molecule-spinner` for the waiting animation. The component applies animations to the login form when the inputs are focused.
 *
 * The component can receive the login event from `cells-credentials-form` and fires it upwards. `loading` boolean attribute can be used to show or hide the spinner.
 *
 * Register button can be hidden or shown using the `allow-registration` attribute.
 *
 * Icons can be passed to the component in order to show clear and toggle icons in the inputs fields (as in `cells-credentials-form`), and start/end icons in the spinner (as in `cells-molecule-spinner`).
 *
 * Use the 'animated' property to apply an initial visual animation to the login and the form or the class animated just to animate the login not the form
 *
 * Example:
 * ```html
 * <cells-login-view
 *   allow-registration
 *   clear-id-icon="coronita:close"
 *   clear-pwd-icon="coronita:close"
 *   toggle-pwd-icon="coronita:visualize"
 *   toggle-pwd-icon-toggled="coronita:hide"
 *   start-icon="coronita:lock"
 *   end-icon="coronita:unlock"
 *   header-image="http://bbva-files.s3.amazonaws.com/cells/assets/glomo/images/access/bg-login.jpg">
 * </cells-login-view>
 * ```
 *
 * The component can optional use inputUserOptions configuration object to validate and mask input user login value.
 *
 * ```html
 * <cells-login-view
 *   allow-registration
 *   clear-id-icon="coronita:close"
 *   clear-pwd-icon="coronita:close"
 *   toggle-pwd-icon="coronita:visualize"
 *   toggle-pwd-icon-toggled="coronita:hide"
 *   start-icon="coronita:lock"
 *   end-icon="coronita:unlock"
 *   input-user-options="[[inputUserOptions]]">
 * </cells-login-view>
 * ```
 * ```js
 *   {
 *     autoValidate: true,
 *     inputStatusValidate: true,
 *     inputType: 'text',
 *     allowedValue: "rut",
 *     errorMessage: 'rutMsg',
 *     errorMessageIcon: 'coronita:error',
 *     mask: 'rut',
 *     maxLength: '13'
 *   }
 *
 * ```
 *
 * An extra login name field (loginName) can be shown in the first step of a two step form if the showUserLoginName option is set.
 *
 * Document types can be configured to use an additional external validation step that interrupts the form flow after step 1 or step 2 when using two steps. See cells-credentials-form.
 *
 * The spinner can also be shown or hidden with the showLoading/hideLoading methods.
 *
 * An additional header image can be configured for the cached user screen. If the waitForCachedUserInitialized option is set, the header image initialization will be deferred until the cached user status is initialized with the setCachedUserInitialized method or by setting a user object.
 *
 * If the goToFirstStepOnError option is set the form will go back from step 2 to step 1 when a login error is notified with the loginError method.
 *
 * ## Animation in iOS devices
 * iOS tries to natively center in screen an input field when it's focused and keyboard is shown. So, running translate animations on input fields depending on focus events can cause the input to be non-centered or even off-screen.
 *
 * To prevent this, `cells-device-behavior` is used to detect iOS devices, and to not apply translate animations in those cases. By default, opacity is changed on the header image when input is focused.
 *
 * ## Icons
 *
 * Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.
 *
 *
 * ## Styling
 *
 * The following custom properties and mixins are available for styling:
 *
 * ### Custom Properties
 * | Custom Property                                        | Selector                                                                | CSS Property                                                | Value                                                                                             |
 * | ------------------------------------------------------ | ----------------------------------------------------------------------- | ----------------------------------------------------------- | ------------------------------------------------------------------------------------------------- |
 * | --bbva-white                                           | :host > --cells-molecule-input-has-content-wrapper:                     | border-bottom                                               |  ![#ffffff](https://placehold.it/15/ffffff/000000?text=+) #ffffff                                 |
 * | --bbva-light-coral                                     | :host > --cells-molecule-input-text-error-message-wrapper:              | color                                                       |  ![#f79698](https://placehold.it/15/f79698/000000?text=+) #f79698                                 |
 * | --bbva-white                                           | :host > --cells-molecule-input-text-error:                              | color                                                       |  ![#ffffff](https://placehold.it/15/ffffff/000000?text=+) #ffffff                                 |
 * | --cells-text-size-14                                   | :host > --cells-molecule-input-text-error:                              | font-size                                                   | `No fallback value`                                                                               |
 * | --cells-login-view-background-color                    | :host                                                                   | background-color                                            | --bbva-core-blue                                                                                  |
 * | --cells-login-view-login-image-height                  | :host                                                                   | --image-height                                              |  40vh                                                                                             |
 * | --image-height                                         | .login-view .header                                                     | height                                                      | `No fallback value`                                                                               |
 * | --bbva-dark-teal                                       | .login-view .header .before                                             | background-color                                            |  ![#02A5A5](https://placehold.it/15/02A5A5/000000?text=+) #02A5A5                                 |
 * | --bbva-white                                           | .login-view .form > --cells-molecule-input-invalid-field-label:         | color                                                       |  ![#fff](https://placehold.it/15/fff/000000?text=+) #fff                                          |
 * | --bbva-medium-blue                                     | .login-view .form > --cells-molecule-input-wrapper:                     | border-bottom                                               |  ![#237ABA](https://placehold.it/15/237ABA/000000?text=+) #237ABA                                 |
 * | --bbva-light-coral                                     | .login-view .form > --cells-molecule-input-invalid-wrapper:             | background-color                                            |  ![#ffffff](https://placehold.it/15/ffffff/000000?text=+) #ffffff                                 |
 * | --bbva-white                                           | .login-view .form > --cells-molecule-input-invalid-wrapper:             | border-bottom                                               |  ![#ffffff](https://placehold.it/15/ffffff/000000?text=+) #ffffff                                 |
 * | --bbva-white                                           | .login-view .form > --cells-credentials-form-fake-selector:             | border-bottom                                               |  ![#237ABA](https://placehold.it/15/237ABA/000000?text=+) #237ABA                                 |
 * | --bbva-white                                           | .login-view .form > --cells-credentials-form-fake-selector-placeholder: | color                                                       | `No fallback value`                                                                               |
 * | --bbva-white                                           | .login-view .form > --cells-molecule-input-field-label:                 | color                                                       |  ![#fff](https://placehold.it/15/fff/000000?text=+) #fff                                          |
 * | --cells-text-size-16                                   | .login-view .form > --cells-molecule-input-field-label:                 | font-size                                                   |  1rem                                                                                             |
 * | --cells-login-view-login-form-user-color               | .login-view .form > --cells-credentials-form-user:                      | color                                                       | --bbva-white                                                                                      |
 * | --cells-login-view-login-form-register-color           | .login-view .form > --cells-credentials-form-register:                  | color                                                       | --bbva-white                                                                                      |
 * | --image-height                                         | .login-view .form                                                       | -webkit-transform                                           | `No fallback value`                                                                               |
 * | --image-height                                         | .login-view .form                                                       | transform                                                   | `No fallback value`                                                                               |
 * | --cells-login-view-background-color                    | .login-view .form                                                       | background-color                                            | --bbva-core-blue                                                                                  |
 * | --cells-login-view-background-image                    | .login-view .form                                                       | background-image                                            |  url("//bbva-files.s3.amazonaws.com/cells/assets/glomo/images/access/fractal-bbva-core-blue.svg") |
 * | --bbva-white                                           | .login-view .form                                                       | --cells-credentials-form-fake-selector-label-selected-color |  ![#ffffff](https://placehold.it/15/ffffff/000000?text=+) #ffffff                                 |
 * | --bbva-core-blue                                       | .login-view .form                                                       | --cells-credentials-form-fake-selector-bg-color             |  ![#004481](https://placehold.it/15/004481/000000?text=+) #004481                                 |
 * | --bbva-white                                           | .login-view .form                                                       | color                                                       |  ![#ffffff](https://placehold.it/15/ffffff/000000?text=+) #ffffff                                 |
 * | --cells-login-view-login-form-input-background-color   | .login-view .form                                                       | --cells-molecule-input-background-color                     | --bbva-core-blue                                                                                  |
 * | --cells-login-view-login-form-input-label-color        | .login-view .form                                                       | --cells-molecule-input-field-label-color                    | --bbva-white                                                                                      |
 * | --cells-login-view-login-form-input-text-color         | .login-view .form                                                       | --cells-molecule-input-field-input-color                    | --bbva-white                                                                                      |
 * | --cells-login-view-login-form-input-icon-color         | .login-view .form                                                       | --cells-molecule-input-button-color                         | --bbva-white                                                                                      |
 * | --cells-login-view-login-form-input-icon-hover-color   | .login-view .form                                                       | --cells-molecule-input-button-hover-color                   | --bbva-white                                                                                      |
 * | --cells-login-view-login-form-input-icon-active-color  | .login-view .form                                                       | --cells-molecule-input-button-active-color                  | --bbva-white                                                                                      |
 * | --cells-login-view-login-form-input-icon-focus-color   | .login-view .form                                                       | --cells-molecule-input-button-focus-color                   | --bbva-white                                                                                      |
 * | --cells-login-view-login-form-submit-disabled-bg-color | .login-view .form                                                       | --cells-st-button-bg-color-secondary-disabled               | --bbva-teal                                                                                       |
 * | --cells-login-view-login-form-link-color               | .login-view .form                                                       | --cells-st-button-text-color-transparent                    | --bbva-white                                                                                      |
 * | --cells-login-view-login-form-link-hover-color         | .login-view .form                                                       | --cells-st-button-text-color-transparent-hover              | --bbva-white                                                                                      |
 * | --cells-login-view-login-form-link-active-color        | .login-view .form                                                       | --cells-st-button-text-color-transparent-active             | --bbva-white                                                                                      |
 * | --cells-login-view-login-image-height-small            | .login-view.small-thumb                                                 | --image-height                                              |  27vh                                                                                             |
 * | --cells-login-view-login-form-register-color           | .login-view.small-thumb .form > --cells-credentials-form-register:      | color                                                       | --bbva-white                                                                                      |
 * | --cells-login-view-spinner-ring-content-color          | .spinner-view                                                           | --cells-molecule-spinner-ring-content-color                 |  rgba(255, 255, 255, 0.2)                                                                         |
 * | --bbva-dark-core-blue                                  | :host(.animated) .login-view .form .before                              | background-color                                            |  ![#043263](https://placehold.it/15/043263/000000?text=+) #043263                                 |
 * | --bbva-core-blue                                       | :host(.animated) .login-view .form .after                               | background-color                                            |  ![#004481](https://placehold.it/15/004481/000000?text=+) #004481                                 |
 * ### @apply
 * | Mixins                                                         | Selector                                                                                | Value |
 * | -------------------------------------------------------------- | --------------------------------------------------------------------------------------- | ----- |
 * | --cells-login-view                                             | :host                                                                                   | {}    |
 * | --cells-login-view-views                                       | .spinner-view                                                                           | {}    |
 * | --cells-login-view-login-view                                  | .login-view                                                                             | {}    |
 * | --cells-login-view-login-header                                | .login-view .header                                                                     | {}    |
 * | --cells-login-view-login-header-before                         | .login-view .header .before                                                             | {}    |
 * | --cells-login-view-login-header-bg                             | .login-view .header-bg                                                                  | {}    |
 * | --cells-fontDefaultLight                                       | .login-view .form > --cells-molecule-input-field-label:                                 | {}    |
 * | --cells-login-view-login-form-input-label                      | .login-view .form > --cells-molecule-input-field-label:                                 | {}    |
 * | --cells-login-view-login-form-submit                           | .login-view .form > --cells-st-button-secondary:                                        | {}    |
 * | --cells-login-view-login-form-user                             | .login-view .form > --cells-credentials-form-user:                                      | {}    |
 * | --cells-login-view-login-form-extra-actions                    | .login-view .form > --cells-credentials-form-extra-actions:                             | {}    |
 * | --cells-login-view-login-form-forgotten-pwd                    | .login-view .form > --cells-credentials-form-forgotten-pwd:                             | {}    |
 * | --cells-login-view-login-form-links                            | .login-view .form > --cells-credentials-form-actions-buttons:                           | {}    |
 * | --cells-login-view-login-form-register-text                    | .login-view .form > --cells-credentials-form-register:                                  | {}    |
 * | --cells-login-view-login-form-submit                           | .login-view .form > --cells-credentials-form-submit:                                    | {}    |
 * | --cells-login-view-login-form-submit                           | .login-view .form > --cells-credentials-form-submit:                                    | {}    |
 * | --cells-login-view-login-form                                  | .login-view .form                                                                       | {}    |
 * | --cells-login-view-login-nonmoveup-maximized                   | .login-view.non-move-up.maximized                                                       | {}    |
 * | --cells-login-view-login-nonmoveup-maximized-header            | .login-view.non-move-up.maximized .header                                               | {}    |
 * | --cells-login-view-login-nonmoveup-maximized-header-before     | .login-view.non-move-up.maximized .header .before                                       | {}    |
 * | --cells-login-view-animated-move-up-maximized-credentials-form | .login-view.non-move-up.maximized cells-credentials-form[animated]                      | {}    |
 * | --cells-login-view-animated-credentials-form                   | cells-credentials-form[animated]                                                        | {}    |
 * | --cells-login-view-login-nonmoveup-maximized-header-bg         | .login-view.non-move-up.maximized .header .header-bg                                    | {}    |
 * | --cells-login-view-login-moveup                                | .login-view.move-up                                                                     | {}    |
 * | --cells-login-view-login-form-extra-actions                    | .login-view.move-up .form > --cells-credentials-form-extra-actions:                     | {}    |
 * | --cells-login-view-login-moveup-form                           | .login-view.move-up .form                                                               | {}    |
 * | --cells-login-view-login-moveup-maximized                      | .login-view.move-up.maximized                                                           | {}    |
 * | --cells-login-view-login-moveup-maximized-header               | .login-view.move-up.maximized .header                                                   | {}    |
 * | --cells-login-view-login-moveup-maximized-header-before        | .login-view.move-up.maximized .header .before                                           | {}    |
 * | --cells-login-view-login-moveup-maximized-header-bg            | .login-view.move-up.maximized .header .header-bg                                        | {}    |
 * | --cells-login-view-login-moveup-maximized-form-form            | .login-view.move-up.maximized .form > --cells-credentials-form-form:                    | {}    |
 * | --cells-login-view-login-moveup-maximized-form-register        | .login-view.move-up.maximized .form > --cells-credentials-form-register:                | {}    |
 * | --cells-login-view-login-form-extra-actions                    | .login-view.move-up.maximized .form > --cells-credentials-form-extra-actions:           | {}    |
 * | --cells-login-view-login-moveup-maximized-form                 | .login-view.move-up.maximized .form                                                     | {}    |
 * | --cells-login-view-login-moveup-maximized-islogged             | .login-view.move-up.maximized.is-logged                                                 | {}    |
 * | --cells-login-view-login-form-extra-actions                    | .login-view.move-up.maximized.is-logged .form > --cells-credentials-form-extra-actions: | {}    |
 * | --cells-login-view-login-moveup-maximized-islogged-form        | .login-view.move-up.maximized.is-logged .form > --cells-credentials-form-form:          | {}    |
 * | --cells-login-view-login-moveup-maximized-islogged-form-form   | .login-view.move-up.maximized.is-logged .form                                           | {}    |
 * | --cells-login-view-login-form-extra-actions                    | .login-view.small-thumb .form > --cells-credentials-form-extra-actions:                 | {}    |
 * | --cells-login-view-spinner                                     | .spinner-view                                                                           | {}    |
 * | --cells-login-view-spinner-spinner                             | .spinner-view .spinner                                                                  | {}    |
 * | --cells-login-view-spinner-spinner                             | .spinner-view .spinner[process]                                                         | {}    |
 * | --cells-login-view-animated-header-before                      | :host(.animated) .login-view .header .before                                            | {}    |
 * | --cells-login-view-animated-header-bg                          | :host(.animated) .login-view .header .header-bg                                         | {}    |
 * | --cells-login-view-animated-cells-credential-form-before-after | :host(.animated) .login-view .form .after                                               | {}    |
 * | --cells-login-view-animated-cells-credential-form-before       | :host(.animated) .login-view .form .before                                              | {}    |
 * | --cells-login-view-animated-cells-credential-form-after        | :host(.animated) .login-view .form .after                                               | {}    |
 * | --cells-login-view-animated-move-up-maximized-header-before    | :host(.animated) .login-view.move-up.maximized .header .before                          | {}    |
 * | --cells-login-view-step-container-step                         | .login-view .form                                                                       | {}    |
 * | --cells-login-view-inputs-input                                | .login-view .form                                                                       | {}    |
 * | --cells-login-view-continue                                    | .login-view .form                                                                       | {}    |
 * | --cells-login-view-go-back                                     | .login-view .form                                                                       | {}    |
 * | --cells-login-view-animated-credentials-form                   | .login-view.move-up cells-credentials-form[animated]                                    | {}    |
 * | --cells-login-view-animated-credentials-form-maximized         | .login-view.move-up.maximized cells-credentials-form[animated]                          | {}    |
 * | --cells-login-view-continue-maximized                          | .login-view.move-up.maximized .form                                                     | {}    |
 *
 * @polymer
 * @cumtomElement
 * @summary contains fields, buttons and links to show a complete login process view. It is mainly composed by a header image, `cells-credentials-form` for the login form, and `cells-molecule-spinner` for the waiting animation. The component applies animations to the login form when the inputs are focused.
 * @extends {Polymer.Element}
 * @demo demo/index.html
 * @hero cells-login-view.png
 * @composer
 * @description Login component.
 * @type UI
 * @platforms android, ios, desktop
 * @family cells-catalog-forms-family
 */
class CellsLoginView extends Polymer.mixinBehaviors([ CellsBehaviors.DeviceBehavior ], Polymer.Element) {
  static get is() {
    return 'cells-login-view';
  }

  static get properties() {
    return {
      /**
       * User to login
       * {
       *  userId: userId,
       *  username: username,
       *  password: password
       * }
       */
      user: {
        type: Object,
        notify: true,
        observer: '_userObserver'
      },
      /**
       * Is the cached user initialized?
       */
      _cachedUserInitialized: {
        type: Boolean,
        value: false
      },
      /**
       * Defer header image loading until cached user status is initialized
       */
      waitForCachedUserInitialized: {
        type: Boolean,
        value: false
      },
      /**
       * Informative messages of process spinner.
       */
      messages: {
        type: Array
      },
      /**
       * Selected Document type.
       */
      documentType: {
        type: Object,
        observer: '_onDocumentTypeSelected'
      },
      /**
       * Checks when image thumb must be smaller
       */
      smallThumb: {
        type: String,
        computed: '_computeSmallThumb(documentType)'
      },
      /**
       * Shows a register button
       */
      allowRegistration: {
        type: Boolean,
        value: false
      },
      /**
       * Step to show the register button
       */
      registrationStep: {
        type: Number,
        value: 2
      },
      /**
       * Show the register button info
       */
      showRegistrationInfo: {
        type: Boolean,
        value: false
      },
      /**
       * Header image URL
       */
      headerImage: {
        type: String,
        value: () => {
          return Polymer.ResolveUrl.resolveUrl('/images/bg-login.jpg');
        }
      },
      /**
       * Header image URL for cached user
       */
      headerImageCachedUser: {
        type: String,
        value: () => {
          return Polymer.ResolveUrl.resolveUrl('/images/bg-login.jpg');
        }
      },
      /**
       * Currently active header image URL
       */
      _currentHeaderImage: {
        type: String,
        computed: '_computeCurrentHeaderImage(headerImage, headerImageCachedUser, user, _cachedUserInitialized, waitForCachedUserInitialized)'
      },
      /**
       * Alt text for header image
       */
      headerImageAlt: {
        type: String,
        value: ''
      },
      /**
       * Alt text for header image for cached user
       */
      headerImageAltCachedUser: {
        type: String,
        value: ''
      },
      /**
       * Currently active alt text for header image
       */
      _currentHeaderImageAlt: {
        type: String,
        computed: '_computeCurrentHeaderImageAlt(headerImageAlt, headerImageAltCachedUser, user, _cachedUserInitialized, waitForCachedUserInitialized)'
      },
      /**
       * Defines if the login is maximized (full screen). This will be set
       * when the inputs are focused.
       */
      maximized: {
        type: Boolean,
        value: false
      },
      /**
       * Defines if username is currently stored
       */
      isLogged: {
        type: Boolean,
        computed: '_isLogged(user.username)'
      },
      /**
       * Defines the loading state.
       */
      loading: {
        type: Boolean,
        value: false
      },
      /**
       * Loading process in spinner
       */
      loadingProcess: {
        type: Boolean,
        value: false
      },
      /**
       * Number of current process
       */
      loadingCurrentProcess: Number,
      /**
       * Delay of first process in spinner
       */
      loadingDelay: {
        type: Number,
        value: 1500
      },
      /**
       * Ends loading login
       */
      loadingEnd: {
        type: Boolean,
        value: false
      },
      /**
       * Size of icons
       */
      iconsSize: {
        type: Number,
        value: 16
      },
      /**
       * Start icon for spinner
       */
      startIcon: {
        type: String
      },
      /**
       * End icon for spinner
       */
      endIcon: {
        type: String
      },
      /**
       * Clear icon for ID field
       */
      clearIdIcon: {
        type: String
      },
      /**
       * Clear icon for Password field
       */
      clearPwdIcon: {
        type: String
      },
      /**
       * Clear icon for Login name field
       */
      clearLoginNameIcon: {
        type: String
      },
      /**
       * Toggle icon for Password field
       */
      togglePwdIcon: {
        type: String
      },
      /**
       * Toggle icon for Password field when it's toggled
       */
      togglePwdIconToggled: {
        type: String
      },
      /**
       * If true, autofocus on first available input
       */
      autofocus: {
        type: Boolean,
        value: false
      },
      /**
       * Cells molecule input validate options
       */
      inputUserOptions: {
        type: Object,
        value: () => ({
          autoValidate: false,
          autoValidatePassword: false,
          inputStatusValidate: false,
          autoMask: true,
          inputType: '',
          allowedValue: '',
          allowedPasswordValue: '',
          errorMessage: '',
          errorPasswordMessage: [],
          errorMessageIcon: '',
          mask: '',
          passwordMask: '',
          maxLength: ''
        })
      },
      /**
       * Secondary id input options
       */
      inputSecondaryIdOptions: {
        type: Object,
        value: () => ({
          autoValidate: false,
          autoValidatePassword: false,
          inputStatusValidate: true,
          inputType: '',
          errorMessage: '',
          errorPasswordMessage: [],
          errorMessageIcon: '',
          min: 5
        })
      },
      /**
       * Delay before navigation on login success, to show the animation
       */
      delayNavigation: {
        type: Number
      },
      /**
       * Disabled reset on-request-change-user
       */
      disableResetOnChangeUser: {
        type: Boolean,
        value: false
      },
      /**
       * Autocapitalize input
       */
      autocapitalize: {
        type: String,
        value: 'none'
      },
      /**
       * Motion to show all the elements
       */
      animated: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },
      /**
       * Allow complete value removal if backspace key is pressed and input type is password
       */
      backspacePwdFullDelete: {
        type: Boolean,
        value: false
      },
      /**
       * If login has more than one step
       */
      steps: {
        type: Boolean,
        value: false
      },
      /**
       * If login has more than one step and first step only has one input
       */
      oneField: {
        type: Boolean,
        value: false
      },
      /**
       * Show user login name input field
       */
      showUserLoginName: {
        type: Boolean,
        value: false
      },
      /**
       * Show secondary id input field
       */
      showSecondaryId: {
        type: Boolean,
        value: false
      },
      /**
       * Hour greeting morning
       */
      hourGreetingMorning: {
        type: Number,
        value: 5
      },
      /**
       * Hour greeting noon
       */
      hourGreetingNoon: {
        type: Number,
        value: 13
      },
      /**
       * Hour greeting night
       */
      hourGreetingNight: {
        type: Number,
        value: 21
      },
      /**
       * Display short user name
       */
      shortUserName: {
        type: Boolean,
        value: false
      },
      /**
       * Move from step 2 to step 1 on login error
       */
      goToFirstStepOnError: {
        type: Boolean,
        value: false
      }
    };
  }

  ready() {
    super.ready();
    this.addEventListener('credentials-external-validation', e => this._onCredentialsExternalValidation(e));
  }

  /**
   * Reset login
   */
  reset() {
    this.animated = false;
    this.loading = false;
    this.user = {};
    this.$.credentialsUserPwd.reset();
    if (this.documentType) {
      this.set('user.documentType', this.documentType);
    }
    this.loadingEnd = false;
    this.maximized = false;
    this.$.spinner.reset();
  }
  /**
   * On login error event
   */
  loginError() {
    this.loading = false;
    this.animated = false;
    this.$.credentialsUserPwd.resetPassword();

    setTimeout(() => {
      this.$.spinner.finish = false;
      this.$.spinner.reset();
    }, this.$.spinner.delay);

    if (!this.isLogged) {
      this.$.credentialsUserPwd.set('gender', null);
    }

    if (!this.isLogged && this.goToFirstStepOnError) {
      setTimeout(() => {
        this.goBack();
      }, 500);
    }
  }
  /**
   * The user data is correct and the login has occurred
   * @param {Object} data
   */
  loadingSuccess(data) {
    this.loadingEnd = true;
    this.$.spinner.finish = true;

    if (this.loadingProcess) {
      this.$.spinner.nextProcess();
    }
    this._cacheUser(data);
    setTimeout(() => {
      this.dispatchEvent(new CustomEvent('navigate-from-login', {
        bubbles: true,
        composed: true
      }));
    }, this.delayNavigation);
    /**
     * @event navigate-from-login
     * Fired when login has occurred
     */
  }
  /**
   * Call spinner next process
   */
  loadingProcessNextStep() {
    this.$.spinner.nextProcess();
  }
  /**
   * On login event
   * @param {Event} e
   */
  _doLogin(e) {
    e.stopPropagation();
    this.cancelDebouncer('restore');
    this.set('user', e.detail);
    this.loading = true;
    this.dispatchEvent(new CustomEvent('login', {
      bubbles: true,
      composed: true,
      detail: e.detail
    }));
    if (this.loadingProcess) {
      this.$.spinner.startProcess();
    }
    /**
     * @event login
     * Fired on login submit
     */
  }
  /**
   * Save username. Next time user only needs to set the password.
   * @param {Object} data
   */
  _cacheUser(data) {
    if (data) {
      this.dispatchEvent(new CustomEvent('register-device', {
        bubbles: true,
        composed: true,
        detail: {
          customerId: this.user.userId,
          firstName: data.firstName,
          lastName: data.lastName,
          loginName: this.user.loginName,
          documentType: this.user.documentType,
          gender: this.user.gender,
          secondaryUserId: this.user.secondaryUserId,
          bank: data.bank || {}
        }
      }));

      if (this.user) {
        this.user.username = data.firstName;
        if (!this.$.credentialsUserPwd.isLogged) {
          this.$.credentialsUserPwd.userName = data.firstName;
          this.$.credentialsUserPwd.lite = false;
          this.$.credentialsUserPwd.goBack();
        }
      }
    }
  }
  /**
   * Remove maximized on blur
   */
  _onInputBlur() {
    if (!this.steps) {
      this.debounce('restore', () => {
        this.maximized = false;
      }, 100);
    }
  }
  /**
   * Add maximized on focus
   */
  _onInputFocus() {
    this.cancelDebouncer('restore');
    this.maximized = true;
  }
  /*
   * On change user
   */
  _changeUser() {
    if (!this.disableResetOnChangeUser) {
      this.reset();
    }
  }
  /*
   * Checked username
   */
  _checkedMaximized(maximized) {
    return maximized ? 'maximized' : '';
  }
  /*
   * Checked username
   */
  _checkedUsername(username) {
    return username ? 'is-logged' : '';
  }
  /*
   * Checks if device is IOS and returns className
   */
  _checkIOS(isIOS) {
    return isIOS ? 'non-move-up' : 'move-up';
  }
  /*
   * Checked username and set isLogged
   */
  _isLogged(username) {
    return username === '-' ? false : !!username;
  }
  /**
   * Sets document type in the user object
   * @param {String} detail
   */
  _onDocumentTypeSelected(detail) {
    if (!this.isLogged) {
      this.$.credentialsUserPwd.set('documentType', detail);
      this.$.credentialsUserPwd.onDocumentTypeSelected();
    }
  }
  /**
   * Computed method to check if the image thumb must be smaller.
   * @param {String} documentType
   * @return {String}
   */
  _computeSmallThumb(documentType) {
    return documentType ? 'small-thumb' : '';
  }
  /*
   * External validation success
   */
  externalValidationSuccess(detail) {
    this.$.credentialsUserPwd.externalValidationSuccess(detail);
  }
  /*
   * Set selected gender
   */
  setGender(gender) {
    this.$.credentialsUserPwd.set('gender', gender);
  }
  /*
   * Moves from step 2 to step 1
   */
  goBack() {
    this.$.credentialsUserPwd.goBack();
  }
  /*
   * Shows the spinner
   */
  showLoading() {
    this.$.spinner.reset();
    this.loading = true;
    this.$.spinner.nextProcess();
  }
  /*
   * Hides the spinner
   */
  hideLoading() {
    this.loading = false;
    this.$.spinner.reset();
  }
  /*
   * Computes the current header image src
   */
  _computeCurrentHeaderImage(headerImage, headerImageCachedUser, user, _cachedUserInitialized, waitForCachedUserInitialized) {
    if (waitForCachedUserInitialized && headerImageCachedUser && !this._cachedUserInitialized) {
      return '';
    }
    return user && user.username ? headerImageCachedUser || headerImage : headerImage;
  }
  /*
   * Computes the current header image alt text
   */
  _computeCurrentHeaderImageAlt(headerImageAlt, headerImageAltCachedUser, user, _cachedUserInitialized, waitForCachedUserInitialized) {
    if (waitForCachedUserInitialized && headerImageAltCachedUser && !this._cachedUserInitialized) {
      return '';
    }
    return user && user.username ? headerImageAltCachedUser || headerImageAlt : headerImageAlt;
  }
  /*
   * Set the cached user initialization status to true
   */
  setCachedUserInitialized() {
    this.set('_cachedUserInitialized', true);
  }
  /**
   * Updates current user data when a document type external validation event is received.
   */
  _onCredentialsExternalValidation(e) {
    //fix: this is needed when using external validations in GloMo because glomo-access-manager is caching the "userName" as "username"
    e.detail.username = e.detail.userName;

    this.set('user', e.detail);
  }
  /**
   * Updates the cached user initialization status.
   */
  _userObserver() {
    this.set('_cachedUserInitialized', true);
  }

  /*
   * Reset after a successful logout
   */
  logoutSuccess() {
    this.hideLoading();
    this.maximized = false;
    this.$.spinner.finish = false;

    this.$.spinner.reset();

    if (this.user) {
      this.user.password = '';
    }
    this.$.credentialsUserPwd.userPassword = '';
  }
}
window.customElements.define(CellsLoginView.is, CellsLoginView);
